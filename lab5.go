package sysprogramming

import (
	"fmt"
	"io"
	"strings"
	"sync"
)

type BinaryNode struct {
	left  *BinaryNode
	right *BinaryNode
	data  int64
}

type BinaryTree struct {
	root *BinaryNode
}

func (t *BinaryTree) insert(data int64) *BinaryTree {
	if t.root == nil {
		t.root = &BinaryNode{data: data, left: nil, right: nil}
	} else {
		t.root.insert(data)
	}
	return t
}

func (n *BinaryNode) insert(data int64) {
	if n == nil {
		return
	} else if data <= n.data {
		if n.left == nil {
			n.left = &BinaryNode{data: data, left: nil, right: nil}
		} else {
			n.left.insert(data)
		}
	} else {
		if n.right == nil {
			n.right = &BinaryNode{data: data, left: nil, right: nil}
		} else {
			n.right.insert(data)
		}
	}
}

func (n *BinaryNode) searchConcurrency(data int64) *BinaryNode {
	if n.data == data {
		return n
	}
	go search(n.left, data)
	go search(n.right, data)
	return n
}

func search(node *BinaryNode, data int64) *BinaryNode {
	if node == nil {
		return nil
	}
	if node.data == data {
		return node
	}
	var result *BinaryNode
	result = search(node.left, data)
	if result == nil {
		result = search(node.right, data)
	}
	return result
}

func printTree(w io.Writer, node *BinaryNode, ns int, ch rune) {
	if node == nil {
		return
	}

	for i := 0; i < ns; i++ {
		_, _ = fmt.Fprint(w, " ")
	}
	_, _ = fmt.Fprintf(w, "%c:%v\n", ch, node.data)
	printTree(w, node.left, ns+2, 'L')
	printTree(w, node.right, ns+2, 'R')
}

func countWordsInString(array []string) int {
	count := 0
	for _ = range array {
		count++
	}
	return count
}

func divideStringToTwoParts(str string) (first []string, second []string) {
	words := strings.Fields(str)
	var len2 int = len(words) / 2
	return words[:len2], words[len2:]
}

func concurrencyCountWords(str string) int {
	array1, array2 := divideStringToTwoParts(str)
	var count1, count2 int
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		count1 = countWordsInString(array1)
		wg.Done()
	}()
	wg.Add(1)
	go func() {
		count2 = countWordsInString(array2)
		wg.Done()
	}()
	wg.Wait()
	return count1 + count2
}