package sysprogramming

import (
	"github.com/mjibson/go-dsp/fft"
	"math"
	"math/cmplx"
)

// Вычисление поворачивающего модуля e^(-i*2*PI*k/N)
func w(k int, N int) complex128 {
	if k%N == 0 {
		return 1
	}
	arg := -2 * math.Pi * float64(k) / float64(N)
	return complex(math.Cos(arg), math.Sin(arg))
}

// Возвращает спектр сигнала
// На вход принимается массив значений сигнала. Количество значений должно быть степенью 2
/*func fft(x []complex128) []complex128 {
	X := make([]complex128, 0)
	N := len(x)
	if N == 2 {
		X = make([]complex128, 2)
		X[0] = x[0] + x[1]
		X[1] = x[0] - x[1]
	} else {
		x_even := make([]complex128, N / 2)
		x_odd := make([]complex128, N / 2)
		for i := 0; i < N / 2; i++ {
			x_even[i] = x[2 * i]
			x_odd[i] = x[2 * i + 1]
		}
		X_even := fft(x_even)
		X_odd := fft(x_odd)
		X = make([]complex128, N)
		for i := 0; i < N / 2; i++ {
			X[i] = X_even[i] + w(i, N) * X_odd[i]
			X[i + N / 2] = X_even[i] - w(i, N) * X_odd[i]
		}
	}
	return X
}*/

// Центровка массива значений полученных в fft (спектральная составляющая при нулевой частоте будет в центре массива)
// На вход принимается массив значений полученный в fft
func nfft(X []complex128) []complex128 {
	N := len(X)
	X_n := make([]complex128, N)
	for i := 0; i < N/2; i++ {
		X_n[i] = X[N/2+i]
		X_n[N/2+i] = X[i]
	}
	return X_n
}

// Вычисление поворачивающего модуля e^(-i*2*PI*k/N)
func w2(k int, N int) complex128 {
	if k%N == 0 {
		return 1
	}
	arg := -2 * math.Pi * float64(k) / float64(N)
	return complex(math.Cos(arg), math.Sin(arg))
}

// Возвращает спектр сигнала
// На вход принимается массив значений сигнала. Количество значений должно быть степенью 2
func fft2(x []complex128) []complex128 {
	for i := range x {
		x[i] = cmplx.Conj(x[i])
	}
	X := fft.FFT(x)
	N := len(X)
	for i := range X {
		X[i] = cmplx.Conj(X[i]) / complex(float64(N), 0)
		//X[i] = X[i] /// complex(float64(N), 0)
		//fmt.Println()
		//X[i] = cmplx.Conj(X[i])
	}
	//fmt.Println(N)
	return X
}

func sumArray(array []float64) float64 {
	var sum float64 = 0
	for _, element := range array {
		sum += element
	}
	return sum
}