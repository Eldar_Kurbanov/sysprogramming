package sysprogramming

import (
	"io/ioutil"
	"path"
)

func subFolderWithMaxSize(dir string) (int64, string, error) {
	var maxSize int64 = 0
	var thisFolderContainsSubFolder = false
	var maxFolder = dir
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return 0, dir, err
	}
	for _, file := range files {
		if file.IsDir() {
			thisFolderContainsSubFolder = true
			localMaxSize, localDir, localErr := subFolderWithMaxSize(path.Join(dir, file.Name()))
			if localErr != nil {
				return 0, file.Name(), localErr
			}
			if localMaxSize > maxSize {
				maxSize = localMaxSize;
				maxFolder = localDir
			}
		}
	}
	if !thisFolderContainsSubFolder {
		for _, file := range files {
			maxSize += file.Size()
		}
	}
	return maxSize, maxFolder, nil
}

func findMaxFile(dir string) (int64, string, error) {
	var maxSize int64 = 0
	var thisFolderContainsSubFolder = false
	var maxFile = dir
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return 0, dir, err
	}
	for _, file := range files {
		if file.IsDir() {
			thisFolderContainsSubFolder = true
			localMaxSize, localFile, localErr := findMaxFile(path.Join(dir, file.Name()))
			if localErr != nil {
				return 0, file.Name(), localErr
			}
			if localMaxSize > maxSize {
				maxSize = localMaxSize;
				maxFile = localFile
			}
		}
	}
	if !thisFolderContainsSubFolder {
		for _, file := range files {
			size := file.Size()
			if size > maxSize {
				maxSize = file.Size()
			}
		}
	}
	return maxSize, maxFile, nil
}