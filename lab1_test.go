package sysprogramming

import (
	"fmt"
	"github.com/mjibson/go-dsp/fft"
	"testing"
)

/*func Test_fft(t *testing.T) {
	testNumbers := make([]complex128, 0)
	testNumbers = append(testNumbers, complex(0, 1), complex(1,2), complex(2, 3), complex(3, 4))
	for _, complexNumber := range testNumbers {
		fmt.Println(complexNumber)
	}
	fmt.Println()
	for _, complexNumber := range fft(testNumbers) {
		fmt.Println(complexNumber)
	}
	fmt.Println()
	//testNumbers2 := fft2(testNumbers)
	for _, complexNumber := range fft2(testNumbers) {
		fmt.Println(complexNumber)
	}
}*/

func TestFFt2(t *testing.T) {
	testNumbers := make([]complex128, 0)
	testNumbers = append(testNumbers, complex(0, 1), complex(1, 2), complex(2, 3), complex(3, 4))
	for _, complexNumber := range testNumbers {
		fmt.Println(complexNumber)
	}
	fmt.Println()
	for _, complexNumber := range fft.FFT(testNumbers) {
		fmt.Println(complexNumber)
	}
	fmt.Println()
	//testNumbers2 := fft2(testNumbers)
	a := fft2(testNumbers)
	fmt.Println(a)
	/*for _, complexNumber := range a { //fft2(testNumbers) {
		fmt.Println(complexNumber)
	}*/
}

func TestDivision(t *testing.T) {
	number1 := complex(4, 2)
	number2 := complex(float64(2), 0)
	fmt.Println(number1 / number2)
}

func TestSumArray(t *testing.T) {
	array := make([]float64, 0)
	array = append(array, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
	sum := sumArray(array)
	if sum != 55 {
		t.Fatal()
	}
}