package sysprogramming

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"sort"
	"time"
)

type Player struct {
	Name string
}

type Command struct {
	Name string
	Players []Player
}

type Match struct {
	Time time.Time
	Location string
	Result1 int
	Result2 int
	Command1 Command
	Command2 Command
}

type Championship struct {
	Matches []Match
}

func (championship *Championship) AddMatch(match Match) {
	championship.Matches = append(championship.Matches, match)
}

func (championship *Championship) RemoveMatch(matchIndex int) {
	championship.Matches = append(championship.Matches[:matchIndex], championship.Matches[matchIndex+1:]...)
}

func (championship *Championship) LoadFromFile(path string) error {
	fileContent, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}
	err = json.Unmarshal(fileContent, championship)
	if err != nil {
		return err
	}
	return nil
}

func (championship *Championship) SaveToFile(path string) error {
	jsonData, err := json.Marshal(championship)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(path, jsonData, 0644)
	if err != nil {
		return err
	}
	return nil
}

func (championship *Championship) GetStandings() string {
	sort.Slice(championship.Matches, func(i, j int) bool {
		return (championship.Matches[i].Result1 + championship.Matches[i].Result2) >
			(championship.Matches[j].Result1 + championship.Matches[j].Result2)
	})
	str := "Location\tCommand1\tResult\tCommand2\tTime\n"
	for _, match := range championship.Matches {
		str += fmt.Sprintf("%s\t%s\t%v : %v\t%s\t%s\n", match.Location, match.Command1.Name,
			match.Result1, match.Result2, match.Command2.Name, match.Time.String())
	}
	return str
}