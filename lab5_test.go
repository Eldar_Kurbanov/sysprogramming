package sysprogramming

import (
	"log"
	"os"
	"testing"
)

func TestE2E(t *testing.T) {
	tree := &BinaryTree{}
	tree.insert(100).
		insert(-20).
		insert(-50).
		insert(-15).
		insert(-60).
		insert(50).
		insert(60).
		insert(55).
		insert(85).
		insert(15).
		insert(5).
		insert(-10)
	printTree(os.Stdout, tree.root, 0, 'M')

	log.Println(search(tree.root, -10))
}

func TestConcurencyCountWords(t *testing.T) {
	count1 := concurrencyCountWords("строка1")
	if count1 != 1 {
		t.Fatal()
	}
	count2 := concurrencyCountWords("строка1 строка2")
	if count2 != 2 {
		t.Fatal()
	}
	count3 := concurrencyCountWords("строка1 строка2 строка3")
	if count3 != 3 {
		t.Fatal()
	}
}