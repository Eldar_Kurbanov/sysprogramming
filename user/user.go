package user

import (
	"bufio"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strings"
)

const (
	userFile string = "/etc/passwd"
)

type Users struct {
	Users []User
}

type User struct {
	Name string
	Directory string
	Group string
	Shell string
}

func ReadEtcPasswd(f string) (list []string) {

	file, err := os.Open(f)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	r := bufio.NewScanner(file)

	for r.Scan() {
		lines := r.Text()
		parts := strings.Split(lines, ":")
		list = append(list, parts[0])
	}
	return list
}

// Check if user on the host
func check(s []string, u string) bool {
	for _, w := range s {
		if u == w {
			return true
		}
	}
	return false
}

// Return securely generated random bytes
func CreateRandom(n int) string {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		fmt.Println(err)
		//os.Exit(1)
	}
	return string(b)
}

// User is created by executing shell command useradd
func AddNewUser(u *User) (string, error) {
	if check(ReadEtcPasswd(userFile), u.Name) {
		return "", errors.New("UserAlreadyExists")
	}
	encrypt := base64.StdEncoding.EncodeToString([]byte(CreateRandom(9)))

	argUser := []string{"-m", "-d", u.Directory, "-G", u.Group, "-s", u.Shell, u.Name}
	argPass := []string{"-c", fmt.Sprintf("echo %s:%s | chpasswd", u.Name, encrypt)}

	userCmd := exec.Command("useradd", argUser...)
	passCmd := exec.Command("/bin/sh", argPass...)

	if out, err := userCmd.Output(); err != nil {
		return "", err
	} else {

		fmt.Printf("Output: %s\n", out)

		if _, err := passCmd.Output(); err != nil {
			return "", err
		}
		return encrypt, nil
	}
}

func RemoveUser(u *User) error {
	if !check(ReadEtcPasswd(userFile), u.Name) {
		return errors.New("UserDoesntExists")
	}

	argCommand := []string{"-r", "-f", u.Name}
	delCmd := exec.Command("userdel", argCommand...)

	if out, err := delCmd.Output(); err != nil {
		return err
	} else {
		fmt.Printf("Output: %s\n", out)

		if _, err := delCmd.Output(); err != nil {
			return err
		}
		return nil
	}
}