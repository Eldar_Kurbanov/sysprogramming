package sysprogramming

import (
	"log"
	"testing"
)

func Test_subFolderWithMaxSize(t *testing.T) {
	maxSize, maxFolder, err := subFolderWithMaxSize("/Volumes/DATA/Games/Riot Games/VALORANT")
	if err != nil {
		t.Fatal(err)
	}
	log.Println(maxSize, maxFolder)
}

func Test_subFileWithMaxSize(t *testing.T) {
	maxSize, maxFile, err := findMaxFile("/Volumes/DATA/Games/Riot Games/VALORANT")
	if err != nil {
		t.Fatal(err)
	}
	log.Println(maxSize, maxFile)
}