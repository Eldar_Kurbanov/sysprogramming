package sysprogramming

import (
	"fmt"
	"testing"
)

func Test_countWordsFromFile(t *testing.T) {
	result, err := countWordsFromFile("lab2_input.txt")
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(result)
}

func Test_getLongerWord(t *testing.T) {
	if getLongerWord("One two three four five six seven eight nine ten") != "eight" {
		t.Fatal()
	}
}