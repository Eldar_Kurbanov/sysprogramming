package sysprogramming

import (
	"fmt"
	"testing"
	"time"
)

func TestChampionship_AddMatch(t *testing.T) {
	var championship Championship
	championship.AddMatch(Match{
		Location: "Volgograd",
		Time: time.Now(),
		Command1: Command{Name: "Gazmyas"},
		Command2: Command{Name: "Rotor"},
		Result1: -1,
		Result2: 0,
	})
	if len(championship.Matches) != 1 {
		t.Fatal()
	}
}

func TestChampionship_RemoveMatch(t *testing.T) {
	var championship Championship
	championship.AddMatch(Match{
		Location: "Volgograd",
		Time: time.Now(),
		Command1: Command{Name: "Gazmyas"},
		Command2: Command{Name: "Rotor"},
		Result1: -1,
		Result2: 0,
	})
	championship.RemoveMatch(0)
	if len(championship.Matches) != 0 {
		t.Fatal()
	}
}

func TestChampionship_FileTest(t *testing.T) {
	var championship Championship
	championship.AddMatch(Match{
		Location: "Volgograd",
		Time: time.Now(),
		Command1: Command{Name: "Gazmyas"},
		Command2: Command{Name: "Rotor"},
		Result1: -1,
		Result2: 0,
	})
	err := championship.SaveToFile("test.json")
	if err != nil {
		t.Fatal(err)
	}
	err = championship.LoadFromFile("test.json")
	if err != nil {
		t.Fatal(err)
	}
}

func TestChampionship_GetStandings(t *testing.T) {
	var championship Championship
	championship.AddMatch(Match{
		Location: "Volgograd",
		Time: time.Now(),
		Command1: Command{Name: "Gazmyas"},
		Command2: Command{Name: "Rotor"},
		Result1: -1,
		Result2: 0,
	})
	championship.AddMatch(Match{
		Location: "Moscow",
		Time: time.Now(),
		Command1: Command{Name: "Zenit"},
		Command2: Command{Name: "Spartak"},
		Result1: 2,
		Result2: 3,
	})
	championship.AddMatch(Match{
		Location: "Vladivostok",
		Time: time.Now(),
		Command1: Command{Name: "VolSU"},
		Command2: Command{Name: "VSTU"},
		Result1: 5,
		Result2: 1,
	})
	fmt.Println(championship.GetStandings())
}