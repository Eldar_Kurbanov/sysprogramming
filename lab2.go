package sysprogramming

import (
	"io/ioutil"
	"strings"
)

func countWordsFromFile(path string) (map[string]int, error) {
	var err error = nil
	content, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	str := string(content)
	result := make(map[string]int)
	words := strings.Fields(str)
	for _, word := range words {
		result[word]++
	}
	return result, nil
}

func getLongerWord(text string) string {
	maxWordInText := 0
	indexOfMaxWord := -1
	words := strings.Fields(text)
	for i, word := range words {
		if (len(word) >= maxWordInText) {
			maxWordInText = len(word)
			indexOfMaxWord = i
		}
	}
	return words[indexOfMaxWord]
}